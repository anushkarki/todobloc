class TodoModel {
  String? id;
  String? title;
  String? desc;

  TodoModel({this.id, this.title, this.desc});

  // todomodel.fromJson()s
  // todomdel.toJson()

  TodoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    desc = json['desc'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['desc'] = desc;
    return data;
  }
}
