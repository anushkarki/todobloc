import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test/cubit/add_todo/addtodo_cubit.dart';
import 'package:test/cubit/update_todo/update_todo_cubit.dart';

import 'cubit/delete_todo/delete_todo_cubit.dart';
import 'presentation/todo.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AddtodoCubit(),
        ),
        BlocProvider(
          create: (context) => DeleteTodoCubit(),
        ),
        BlocProvider(
          create: (context) => UpdateTodoCubit(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: TodoScreen(),
      ),
    );
  }
}
