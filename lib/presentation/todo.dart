import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test/cubit/add_todo/addtodo_cubit.dart';
import 'package:test/cubit/update_todo/update_todo_cubit.dart';

import '../cubit/delete_todo/delete_todo_cubit.dart';
import '../data/models/todo_model.dart';
import 'widgets/textfield.dart';

class TodoScreen extends StatelessWidget {
  TodoScreen({super.key});
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _handleAddList(context);
        },
        child: const Icon(Icons.add),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 15,
          top: 50,
          right: 15,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "My TODOs:",
              style: GoogleFonts.poppins(
                fontSize: 30,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            StreamBuilder(
              stream:
                  FirebaseFirestore.instance.collection('todolist').snapshots(),
              builder: (context, snapshot) {
                return Expanded(
                  child: ListView.separated(
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                    itemCount: snapshot.data!.size,
                    separatorBuilder: (context, index) => const SizedBox(
                      height: 10,
                    ),
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () => _handleUpdate(context,
                            id: snapshot.data!.docs[index].id,
                            title: snapshot.data!.docs[index].data()["title"],
                            description:
                                snapshot.data!.docs[index].data()["desc"]),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.fromLTRB(30, 10, 20, 10),
                          decoration: BoxDecoration(
                            gradient: const LinearGradient(
                                colors: [Color(0xff49a9fc), Color(0xff286eff)]),
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 4,
                                offset: const Offset(0, 4),
                                color: Colors.black.withOpacity(0.4),
                              )
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      snapshot.data!.docs[index]
                                          .data()["title"],
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: GoogleFonts.poppins(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                    ),
                                    Text(
                                      snapshot.data!.docs[index].data()["desc"],
                                      maxLines: 5,
                                      overflow: TextOverflow.ellipsis,
                                      style: GoogleFonts.roboto(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              BlocConsumer<DeleteTodoCubit, DeleteTodoState>(
                                listener: (context, state) {
                                  if (state is DeleteTodoLoading) {
                                    const CircularProgressIndicator();
                                  }
                                  if (state is DeleteTodoComplete) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        const SnackBar(
                                            content:
                                                Text("Deleted Successfully!")));
                                  }
                                  if (state is DeleteTodoFailed) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(content: Text(state.error)));
                                  }
                                },
                                builder: (context, state) {
                                  return GestureDetector(
                                    onLongPress: () {
                                      BlocProvider.of<DeleteTodoCubit>(context)
                                          .deleteTodo(
                                        snapshot.data!.docs[index].id,
                                      );
                                    },
                                    child: const Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> _handleAddList(BuildContext context) {
    _titleController.clear();
    _descController.clear();
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 40),
              child: Column(
                children: [
                  MyTextField(
                    titleController: _titleController,
                    hintText: "Title",
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  MyTextField(
                    titleController: _descController,
                    hintText: "Description",
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  BlocConsumer<AddtodoCubit, AddtodoState>(
                    listener: (context, state) {
                      if (state is AddTodoLoading) {
                        const CircularProgressIndicator();
                      }
                      if (state is AddTodoCompleted) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text("Todo List Added")));
                      }
                      if (state is AddTodoFailed) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text(state.error)));
                      }
                    },
                    builder: (context, state) {
                      return GestureDetector(
                        onTap: () {
                          if (_titleController.text.isEmpty &&
                              _descController.text.isEmpty) return;
                          TodoModel model = TodoModel(
                              title: _titleController.text,
                              desc: _descController.text);
                          BlocProvider.of<AddtodoCubit>(context).addTodo(model);
                          _titleController.clear();
                          _descController.clear();
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          width: MediaQuery.of(context).size.width,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: const LinearGradient(
                                colors: [Color(0xff49a9fc), Color(0xff286eff)]),
                          ),
                          child: Center(
                            child: Text(
                              "Submit",
                              style: GoogleFonts.poppins(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Future<dynamic> _handleUpdate(BuildContext context,
      {required id, String? title, String? description}) {
    _titleController.text = title ?? "";
    _descController.text = description ?? "";
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 40),
              child: Column(
                children: [
                  MyTextField(
                    titleController: _titleController,
                    hintText: "Title",
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  MyTextField(
                    titleController: _descController,
                    hintText: "Description",
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  BlocConsumer<UpdateTodoCubit, UpdateTodoState>(
                    listener: (context, state) {
                      if (state is UpdateTodoLoading) {
                        const CircularProgressIndicator();
                      }
                      if (state is UpdateTodoComplete) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text("Todo List Updated")));
                      }
                      if (state is UpdateTodoFailed) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text(state.error)));
                      }
                    },
                    builder: (context, state) {
                      return GestureDetector(
                        onTap: () {
                          if (_titleController.text.isEmpty &&
                              _descController.text.isEmpty) return;
                          TodoModel model = TodoModel(
                              title: _titleController.text,
                              desc: _descController.text);
                          BlocProvider.of<UpdateTodoCubit>(context)
                              .updateTodo(id: id, data: model);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          width: MediaQuery.of(context).size.width,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: const LinearGradient(
                                colors: [Color(0xff49a9fc), Color(0xff286eff)]),
                          ),
                          child: Center(
                            child: Text(
                              "Update",
                              style: GoogleFonts.poppins(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
