part of 'update_todo_cubit.dart';

abstract class UpdateTodoState extends Equatable {
  const UpdateTodoState();

  @override
  List<Object> get props => [];
}

class UpdateTodoInitial extends UpdateTodoState {}

class UpdateTodoLoading extends UpdateTodoState {}

class UpdateTodoComplete extends UpdateTodoState {}

class UpdateTodoFailed extends UpdateTodoState {
  const UpdateTodoFailed({required this.error});
  final String error;
}
