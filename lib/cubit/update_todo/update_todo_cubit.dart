import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:test/data/models/todo_model.dart';

part 'update_todo_state.dart';

class UpdateTodoCubit extends Cubit<UpdateTodoState> {
  UpdateTodoCubit() : super(UpdateTodoInitial());

  updateTodo({required id, required TodoModel data}) async {
    try {
      emit(UpdateTodoLoading());
      DocumentReference document =
          FirebaseFirestore.instance.collection('todolist').doc(id);

      await document.update({"title": data.title, "desc": data.desc});
      emit(UpdateTodoComplete());
    } on FirebaseException catch (e) {
      emit(
        UpdateTodoFailed(
          error: e.message!,
        ),
      );
    }
  }
}
