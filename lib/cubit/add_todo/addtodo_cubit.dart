import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:test/data/models/todo_model.dart';

part 'addtodo_state.dart';

class AddtodoCubit extends Cubit<AddtodoState> {
  AddtodoCubit() : super(AddtodoInitial());

  // TodoRepository repository = TodoRepository();

  addTodo(TodoModel model) async {
    emit(AddTodoLoading());
    try {
      CollectionReference collectionReference =
          FirebaseFirestore.instance.collection("todolist");
      await collectionReference.add({
        "id": collectionReference.doc().id,
        "title": model.title,
        "desc": model.desc,
      });
      emit(AddTodoCompleted());
    } on FirebaseException catch (e) {
      emit(AddTodoFailed(error: e.message!));
    }
  }
}
