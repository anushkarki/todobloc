part of 'addtodo_cubit.dart';

abstract class AddtodoState extends Equatable {
  const AddtodoState();

  @override
  List<Object> get props => [];
}

class AddtodoInitial extends AddtodoState {
  
}

class AddTodoLoading extends AddtodoState {}

class AddTodoCompleted extends AddtodoState {}

class AddTodoFailed extends AddtodoState {
  final String error;
  const AddTodoFailed({required this.error});
}
