part of 'delete_todo_cubit.dart';

abstract class DeleteTodoState extends Equatable {
  const DeleteTodoState();

  @override
  List<Object> get props => [];
}

class DeleteTodoInitial extends DeleteTodoState {}

class DeleteTodoLoading extends DeleteTodoState {}

class DeleteTodoComplete extends DeleteTodoState {}

class DeleteTodoFailed extends DeleteTodoState {
  const DeleteTodoFailed({required this.error});
  final String error;
}
