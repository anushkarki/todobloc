import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

part 'delete_todo_state.dart';

class DeleteTodoCubit extends Cubit<DeleteTodoState> {
  DeleteTodoCubit() : super(DeleteTodoInitial());

  deleteTodo(id) async {
    try {
      emit(DeleteTodoLoading());
      DocumentReference document =
          FirebaseFirestore.instance.collection('todolist').doc(id);

      await document.delete();
      emit(DeleteTodoComplete());
    } on FirebaseException catch (e) {
      emit(
        DeleteTodoFailed(
          error: e.message!,
        ),
      );
    }
  }
}
